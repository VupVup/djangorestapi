from django.shortcuts import render, get_object_or_404
from rest_framework.exceptions import NotFound
from rest_framework import generics
import requests
from django.contrib.auth.models import User
from .serializers import (TaskSerializer, TasklistSerializer, TagSeializer, UserRegistrationSerializer, 
                          TokenSerializer, UserConfirmSerializer, KostylSerialiser)
from django.http import HttpResponseRedirect
from .models import Task, Tasklist, Tag, LoggedInMixin, UserProfile
from rest_framework.authtoken.models import Token
from rest_framework import permissions
from django.contrib import auth


class IsNotAuthenticated(permissions.BasePermission):
    def has_permission(self, request, view):
        return not request.user.is_authenticated()

class TagCreateView(generics.ListCreateAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagSeializer

class TasklistsharedView(generics.ListCreateAPIView):
    serializer_class = TasklistSerializer
    def get_queryset(self):
        queryset =  Tasklist.objects.filter(friends=self.request.user)
        return queryset


class TasklistCreateView(generics.ListCreateAPIView):
    serializer_class = TasklistSerializer

    def get_queryset(self):
        queryset = Tasklist.objects.filter(owner=self.request.user)
        return queryset

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class TasklistDetailsView(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return Tasklist.objects.filter(owner=self.request.user).filter(pk=self.kwargs.get('pk', None))
    serializer_class = TasklistSerializer


class TaskCreateView(generics.ListCreateAPIView):
    serializer_class = TaskSerializer

    def get_queryset(self):
        queryset = Task.objects.filter(tasklist__owner=self.request.user)
        return queryset

    def perform_create(self, serializer):
        serializer.save()

class TaskCreateInListView(generics.ListCreateAPIView):
    serializer_class = TaskSerializer

    def get_queryset(self):
        if self.request.user.is_authenticated():
            queryset = Task.objects.all()
            list_id = self.kwargs.get('list_id', None)
            if list_id is not None:
                queryset = queryset.filter(tasklist_id = list_id).filter(tasklist__owner=self.request.user)
        else:
            queryset = []
        return queryset

    def perform_create(self, serializer):
        list_id = self.kwargs.get('list_id', None)
        try:
            tasklist = Tasklist.objects.get(pk=list_id)
        except Tasklist.DoesNotExist:
            raise NotFound()
        serializer.save(tasklist=tasklist)


class TaskDetailsView(LoggedInMixin, generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TaskSerializer

    def get_queryset(self):
        queryset = Task.objects.all()
        list_id = self.kwargs.get('list_id', None)
        if list_id is not None:
            queryset = queryset.filter(tasklist_id = list_id).filter(tasklist__owner=self.request.user)
        return queryset

class UserCreateView(generics.ListCreateAPIView):
    #permission_classes = (IsNotAuthenticated,)
    serializer_class = UserRegistrationSerializer
    def get_queryset(self):
        queryset = User.objects.all()
        return queryset

    
class Tokens(generics.ListCreateAPIView):
    serializer_class = TokenSerializer
    #for t in Token.objects.all():
    #    token_user = Token_user.objects.create(
    #        token = t.key,
    #        user_id = t.user.id
    #        )
    #    token_user.save()
    #queryset = Token_user.objects.all()
    def get_queryset(self):
        idx = self.request.user
        print (idx.id)
        queryset = Token.objects.all()   # filter(user_id=idx.id)
        return queryset

#class Keys(generics.CreateAPIView):
#    serializer_class = UserConfirmSerializer
#    queryset = UserProfile.objects.all()
#        #idx = UserProfile.objects.filter(activation_key=activation_key).user_id

def activate(request, activation_key):
    idx = get_object_or_404(UserProfile, activation_key=activation_key)
    print(idx)
    u = User.objects.get(id=idx.user_id)
    u.is_active = True
    u.save()
    return render(request, 'confirmtpl.html')